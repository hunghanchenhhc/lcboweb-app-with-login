package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		if( loginName ==null || loginName.matches(".*\\s.*"))
		{
			return false;
		}  
		
		String line = loginName;
	      String pattern = "^[A-Za-z0-9]{6,}";

	      Pattern r = Pattern.compile(pattern);

	      Matcher m = r.matcher(line);
	      if (m.find( ) ) {
	         return true;
	      }
	      return false;
	}
	
}
