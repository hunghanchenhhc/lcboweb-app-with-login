package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "aaa00a" ) );
	}
	@Test
	public void testIsValidLoginException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( null ) );
	}
	@Test
	public void testIsValidLoginBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "AAAAAA" ) );
	}
	@Test
	public void testIsValidLoginBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "000  0" ) );
	}
	
	
	@Test
	public void testIsValidLoginNameCountRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "chen9915" ) );
	}
	
	@Test
	public void testIsValidLoginNameCountException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( null ) );
	}
	@Test
	public void testIsValidLoginNameCountBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "a23456" ) );
	}
	@Test
	public void testIsValidLoginNameCountBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "12 456" ) );
	}
}
